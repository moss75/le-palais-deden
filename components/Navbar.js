import Link from "next/link";

export default function Navbar(){
    return (
        <nav className="text-2xl font-medium z-40 relative">
            <ul className="flex">
                <Link href={"/"}>
                    <li>Home</li>
                </Link>
                <Link href={"/contact"}>
                    <li className="mx-3">Contact</li>
                </Link>
                <Link href={"/menu"}>
                    <li className="mx-3">Menu</li>
                </Link>
            </ul>
        </nav>
    )
}