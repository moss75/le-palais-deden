import { motion as m } from 'framer-motion';
import Link from "next/link";
import Image from "next/image";
import brick from './../../public/brick.jpg';
import toast from './../../public/toast2.png';


export default function Entrees(){
    return (
        <m.div
            initial={{x: "100%"}}
            animate={{x: "0%"}}
            transition={{duration: 0.75, ease:"easeOut"}}
            exit={{opacity: 1}}
            className="text-gray-900 absolute top-0 left-0 w-full h-full "
        >
            <main className="text-gray-900 absolute top-0 left-0 w-full h-full itemBg p-5 lg:overflow-hidden">
                <div className="my-60 p-1">
                    <Link href="/menu">
                        <h1 className="text-5xl mb-5"> Retour </h1>
                    </Link>
                    <div className="lg:flex gap-36">
                        <m.div
                            className="relative"
                            initial={{y: "100%", opacity: 0}}
                            animate={{y: "0%", opacity: 1}}
                            transition={{delay: 0.5,duration: 0.75, ease:"easeOut"}}
                            exit={{opacity: 1}}


                        >
                            <Image src={brick} width={500} height={500}/>
                            <p className="absolute bottom-10 -right-5 text-3xl">Brick au thon</p>
                            <p className="absolute -bottom-2 -right-20 text-3xl">BlaBla Sur la recette </p>
                        </m.div>
                        <m.div
                            className="relative"
                            initial={{y: "150%"}}
                            animate={{y: "0%"}}
                            transition={{delay: 0.5,duration: 0.75, ease:"easeOut"}}
                        >
                            <p className="absolute bottom-10 -right-5 text-3xl">Brick au thon</p>
                            <p className="absolute -bottom-2 -right-20 text-3xl">BlaBla Sur la recette </p>
                            <Image src={toast} width={500} height={500}/>
                        </m.div>
                    </div>
                </div>
            </main>
        </m.div>
    )
}