import { motion as m } from 'framer-motion';


export default function Contact(){
    return (
        <m.div
            initial={{y: "100%"}}
            animate={{y: "0%"}}
            transition={{duration: 0.75, ease:"easeOut"}}
            exit={{opacity: 1}}
            className="text-gray-900 absolute top-0 left-0 w-full h-full bg-orange-100 lg:px-48 px-16"
        >
            <main className="text-gray-900 absolute top-0 left-0 w-full h-full bg-red-400 lg:px-48 px-16">
                <div className="my-96 p-1">
                    <h1>Lets talk</h1>
                </div>
                <div className="flex gap-40">
                    <h4>Find me:</h4>
                    <div className="lg:text-6xl text-2xl underline">
                        <ul>
                            <li className="pb-2">Twitter</li>
                            <li className="pb-2">snapchat</li>
                        </ul>
                    </div>
                </div>
            </main>
        </m.div>
    )
}