import { motion as m } from 'framer-motion';
import Image from "next/image";
import brick from './../../public/brick.jpg';
import Link from "next/link";


export default function Menu(){
    return (
        <m.div
            initial={{y: "100%"}}
            animate={{y: "0%"}}
            transition={{duration: 0.75, ease:"easeOut"}}
            exit={{opacity: 1}}
            className="text-gray-900 absolute top-0 left-0 w-full h-full lg:px-48 px-16 p-1 overflow-hidden"
        >
            <div className="text-gray-900 absolute top-0 left-0 w-full h-full lg:px-48 px-16 menuBg">
                    <h1 className="text-6xl   text-center lg:text-9xl z-10 mt-36">Menu à composer</h1>
                <m.p
                    className="text-left mt-6  "
                    initial={{y: "100%"}}
                    animate={{y: "0%"}}
                    transition={{duration: 0.75, ease:"easeOut"}}
                    exit={{opacity: 1}}

                >Composer Votre menu parmi nos entrées, plats, desserts et boissons faits maison, Vente à emporter, Commande sur snapchat , Instagram, ou en suivant ce lien:</m.p>
                <div className="lg:flex lg:gap-40 overflow-hidden p-0 relative">
                    <div
                        className="basis-3/12 text-2xl overflow-hidden border-solid border-orange-900"
                    >
                        <h1 className="  text-center text-4xl mt-10">Menu</h1>
                        <Link href={"entrees"}>
                            <h1 className="  text-center text-6xl mt-10">Entrées</h1>
                        </Link>
                        <h1 className="  text-center text-6xl mt-10">Plats</h1>

                        <h1 className="  text-center text-6xl mt-10">Desserts</h1>
                        <h1 className="  text-center text-6xl mt-10">Boissons</h1>

                    </div>
                    <m.div
                        className=" basis-9/12 rounded-lg p-8 overflow-hidden"
                        initial={{y: "100%"}}
                        animate={{y: "0%"}}
                        transition={{delay: 0.5,duration: 0.75, ease:"easeOut"}}
                        exit={{opacity: 1}}

                    >
                        <div className="relative" >
                            <Image
                                src={brick}
                                objectFit="cover"
                                alt="brick"
                            ></Image>
                            <div>
                                <p className="absolute top-20 -right-3 text-3xl">instagram</p>
                            </div>
                        </div>

                    </m.div>
                </div>
            </div>
        </m.div>
    )
}