import Image from 'next/image';
import bg from './../../public/HomeCook.jpg';
import { Inter } from 'next/font/google';
import { motion as m } from 'framer-motion';
import palace from './../../public/palace.png'
import Link from "next/link";



const inter = Inter({ subsets: ['latin'] })

export default function Home() { 
  return (
      <m.main
          initial={{y: "100%"}}
          animate={{y: "0%"}}
          exit={{opacity: 1}}
          transition={{duration: 0.75, ease:"easeOut"}}
          className="text-gray-900 absolute top-0 left-0 w-full h-full bg-stone-900 overflow-hidden"
      >
          <m.div className="absolute w-full h-full z-0"
                 initial={{opacity: 0, scale: 1}}
                 animate={{opacity: 0.5, scale: 1.05}}
                 exit={{opacity: 0}}
                 transition={{delay: 0.5,duration: 0.5}}
          >
              <Image
              src={bg}
              alt="background"
              layout="fill"
              objectFit="cover"


          />


          </m.div>
              <div className="my-96 p-1 overflow-hidden lg:px-48 px-16 ">
                  <Image src={palace} className="absolute top-60 left-5"/>
                  <m.h1
                      className="text-6xl text-center lg:text-right lg:text-9xl z-10"
                      initial={{y: "100%"}}
                      animate={{y: 0}}
                      exit={{opacity: 0}}
                      transition={{delay: 0.5,duration: 0.5}}
                  >
                      Palais d'Eden
                  </m.h1>
              </div>

          <Link href={"/menu"}>
              <h2 className="text-3xl text-center lg:text-right lg:text-6xl z-10 mt-4">Voir le menu</h2>
          </Link>

      </m.main>
  )
}
